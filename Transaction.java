package Core;

import java.util.Date;
import java.util.List;
import java.util.Vector;

public class Transaction {

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<DetailTransaction> getItem() {
        return item;
    }

    public void setItem(List<DetailTransaction> item) {
        this.item = item;
    }

    public static class DetailTransaction
    {
        private Product product;
        private int qty;
        private int price;

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }
     
        
    }

    private String transactionNumber;
    private Date transactionDate;
    private Customer customer;
    private List<DetailTransaction> item = new Vector<DetailTransaction>();

    public List<Transaction> select()
    {
        return new DataSource().selectTransactionFromDB();
    }

    public void insert()
    {
        new DataSource().insertTransactionToDB(this);
    }

    public void update()
    {
        new DataSource().updateTransactionToDB(this);
    }

    public void remove()
    {
        new DataSource().removeTransactionFromDB(this);
    }
}
